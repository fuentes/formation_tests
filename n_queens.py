import doctest


def is_free(σ, i, j):
    """
    is_free check is (i,j) is a free position to put another queen"
    >>> from numpy import zeros, array, reshape
    >>> from numpy.linalg import norm
    >>> a =  array([[0., 0., 0., 0., 0., 0., 0., 0.],\
                   [0., 0., 0., 0., 0., 0., 0., 0.],\
                   [0., 0., 0., 0., 0., 0., 0., 0.],\
                   [0., 1., 0., 0., 0., 0., 0., 0.],\
                   [0., 1., 0., 1., 0., 0., 0., 1.],\
                   [0., 1., 0., 0., 1., 0., 0., 1.],\
                   [0., 1., 0., 1., 1., 1., 0., 0.],\
                   [0., 0., 0., 1., 1., 1., 0., 0.]])
    >>> norm(array([1.*is_free([0,2,6,-1, -1, -1, -1, -1], i,j) \
             for i in range(8) for j in range(8)]).reshape(8,8)-a ) <1e-12
    True
    """
    ok = True
    n = len(σ)
    for line in range(n):
        c = σ[line]
        if c != -1:
            if c > line:
                val = ((j-i) != (c - line))
            else:
                val = (i-j) != (line - c)
            val &= (c != j) & (line != i) & (i+j != (c+line))
            ok &= val
        line += 1
    return ok


def place_queen(σ, i, solutions):
    n = len(σ)
    if i > n - 1:
        solutions.append(σ.copy())
    else:
        col = 0
        while col < n:
            if is_free(σ, i, col):
                σ[i] = col
                place_queen(σ, i + 1, solutions)
                σ[i] = -1
            col += 1


def solve_n_queens(n):
    solutions = []
    place_queen([-1] * n, 0, solutions)
    return solutions

doctest.testmod()
